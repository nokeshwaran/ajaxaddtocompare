<?php

namespace OX\AjaxAddtoCompare\Plugin;

use Magento\Catalog\Helper\Product\Compare;
use Magento\Catalog\CustomerData\CompareProducts as CompareProductObj;

/**
 * Omit email sending depending on the system configuration setting
 *
 * @param CompareProductObj $subject
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
class CompareProducts
{
    protected $helper;

    public function __construct(
        Compare $helper
    ) {
        $this->helper = $helper;
    }

    public function afterGetSectionData(CompareProductObj $subject, $result)
    {
        $result['itemIds'] = $result['count'] ? $this->getItemsIds() : [];
        return $result;
    }

    protected function getItemsIds()
    {
        $items = [];
        foreach ($this->helper->getItemCollection() as $item) {
            $items[$item->getId()] = $this->helper->getPostDataRemove($item);
        }
        return $items;
    }
}
