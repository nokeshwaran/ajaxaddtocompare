<?php

namespace OX\AjaxAddtoCompare\Controller\Compare;

use Exception;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Controller\Product\Compare;
use Magento\Catalog\Helper\Product\Compare as CompareHelper;
use Magento\Catalog\Model\Product\Compare\ItemFactory;
use Magento\Catalog\Model\Product\Compare\ListCompare;
use Magento\Catalog\Model\ResourceModel\Product\Compare\Item\Collection;
use Magento\Catalog\Model\ResourceModel\Product\Compare\Item\CollectionFactory;
use Magento\Catalog\Model\Session;
use Magento\Customer\Model\Session as customerSession;
use Magento\Customer\Model\Visitor;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface as HttpPostActionInterface;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Escaper;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Result\PageFactory;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Unit test for Consumer class.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ElseExpression)
 */
class Index extends Compare implements HttpPostActionInterface
{
    /**
     * @var Compare
     */
    protected $helper;
    /**
     * @var Escaper
     */
    protected $escaper;

    protected $productName;

    /**
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     * @param Context $context
     * @param ItemFactory $compareItemFactory
     * @param CollectionFactory $itemsFactory
     * @param customerSession $customerSession
     * @param Visitor $customerVisitor
     * @param ListCompare $productCompareList
     * @param Session $catalogSession
     * @param StoreManagerInterface $storeManager
     * @param Validator $formKeyValidator
     * @param PageFactory $resultPageFactory
     * @param ProductRepositoryInterface $productRepository
     * @param CompareHelper $helper
     * @param Escaper $escaper
     */
    public function __construct(
        Context $context,
        ItemFactory $compareItemFactory,
        CollectionFactory $itemsFactory,
        customerSession $customerSession,
        Visitor $customerVisitor,
        ListCompare $productCompareList,
        Session $catalogSession,
        StoreManagerInterface $storeManager,
        Validator $formKeyValidator,
        PageFactory $resultPageFactory,
        ProductRepositoryInterface $productRepository,
        CompareHelper $helper,
        Escaper $escaper
    ) {
        parent::__construct(
            $context,
            $compareItemFactory,
            $itemsFactory,
            $customerSession,
            $customerVisitor,
            $productCompareList,
            $catalogSession,
            $storeManager,
            $formKeyValidator,
            $resultPageFactory,
            $productRepository
        );
        $this->helper = $helper;
        $this->escaper = $escaper;
    }

    public function execute()
    {
        $productId = $this->getRequest()->getParam('productId');
        $storeId = $this->_storeManager->getStore()->getId();
        try {
            /** @var Product $product */
            if ($productId) {
                $product = $this->productRepository->getById($productId, false, $storeId);
                $this->productName = $this->escaper->escapeHtml($product->getName());
                $items = $this->_compareItemFactory->create();
                if ($this->_customerSession->isLoggedIn()) {
                    $items->setCustomerId($this->_customerSession->getCustomerId());
                } elseif ($this->_customerId) {
                    $items->setCustomerId($this->_customerId);
                } else {
                    $items->setVisitorId($this->_customerVisitor->getId());
                }
            }
        } catch (NoSuchEntityException $e) {
            $product = null;
        }

        if ($this->getRequest()->getParam('clearAll') == 'true'):
            $this->clearAllItems();
        else:

                $compareitem = $this->helper->getItemCollection()
                ->addFieldToFilter('entity_id', ['eq' => $productId])->getData();

                $compareitem
                    ? $this->removeCompareItems($items->loadByProduct($product))
                    : $this->addCompareItem($product);
        endif;
            $this->helper->calculate();
    }

    public function clearAllItems()
    {
        /** @var Collection $items */
        $items = $this->_itemCollectionFactory->create();

        if ($this->_customerSession->isLoggedIn()) {
            $items->setCustomerId($this->_customerSession->getCustomerId());
        } elseif ($this->_customerId) {
            $items->setCustomerId($this->_customerId);
        } else {
            $items->setVisitorId($this->_customerVisitor->getId());
        }

        try {
            $items->clear();
            $this->messageManager->addSuccessMessage(__('You cleared the comparison list.'));
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('Something went wrong  clearing the comparison list.'));
        }
    }

    public function removeCompareItems($item)
    {
        if ($item->getId()) {
            $item->delete();
            $this->messageManager->addSuccessMessage(
                __('You removed product %1 from the comparison list.', $this->productName)
            );
            $this->_eventManager->dispatch(
                'catalog_product_compare_remove_product',
                ['product' => $item]
            );
        }
    }

    public function addCompareItem($product)
    {
        $this->_catalogProductCompareList->addProduct($product);
        $this->messageManager->addComplexSuccessMessage(
            'addCompareSuccessMessage',
            [
                'product_name' => $this->productName,
                'compare_list_url' => $this->_url->getUrl('catalog/product_compare'),
            ]
        );

        $this->_eventManager->dispatch('catalog_product_compare_add_product', ['product' => $product]);
    }
}
