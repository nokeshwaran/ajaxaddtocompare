<?php

namespace OX\AjaxAddtoCompare\ViewModel;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use OX\AjaxAddtoCompare\Helper\Data;

class AjaxCompareStatus implements ArgumentInterface
{
    protected $helperData;
    protected $request;

    public function __construct(Data $helperData, Context $context)
    {
        $this->helperData = $helperData;
        $this->request = $context->getRequest();
    }

    public function moduleStatus()
    {
        return $this->helperData->isModuleEnabled();
    }

//    public function isWishlistPage()
//    {
//
//        return $this -> request -> getFullActionName() == "wishlist_index_index";
//    }
}
