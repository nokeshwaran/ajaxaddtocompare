define([
    'jquery',
    'mage/url',
    'jquery-ui-modules/widget',
    'mage/cookies'

], function ($, urlBuilder) {
    'use strict';
    var self;
    $.widget('mage.ajaxWishlist', {

        _create: function () {
            this._bind();
        },

        _bind: function () {
            self = this;
            $('body').on('click', '[data-action="compare-actions"]', this._compareAction);
        },

        _compareAction: function (event) {

            var productId = $(this).attr('data-product-id'),
                clearAll = $(this).attr('data-clear-all'),

                params = {
                    productId: productId,
                    clearAll: typeof $(this).attr('data-clear-all') !== "undefined"
                };

            self._ajaxcall(params)
        },

        _ajaxcall: function (params) {


            $.ajax({
                type: "POST",
                dataType: "json",
                url: urlBuilder.build("ajaxcompare/compare/index"),
                data: params
            }).done(function (data) {
            });
        }

    });
    return $.mage.ajaxWishlist;
});
