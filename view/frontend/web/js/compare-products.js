define([

    'jquery',
    'Magento_Customer/js/customer-data',
    'uiComponent',
    'ko'
], function ($, customerData, Component, ko) {
    'use strict';
    return Component.extend({

        compareItems: ko.observable(),


        initialize: function () {
            this._super();
            this.compareItems = customerData.get('compare-products');
        },


    });
});
